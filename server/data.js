function index(enviroment, callback, context) {
  var data = {
    label: context.label
  };

  callback(null, data);
}


module.exports = {
  index: index
};