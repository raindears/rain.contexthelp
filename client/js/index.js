define([], function() {

  function ContextHelp() {}

  ContextHelp.prototype = {

    init: function() {},

    start: function() {
      this._element = this.context.getRoot().find('.contexthelp');
    }

  };

  ContextHelp.prototype.label = function (value) {
    if (!this._element) {
      throw new RainError('label() cannot be called before start is executed',
        RainError.ERROR_PRECONDITION_FAILED);
    }

    var label = this._element.find('.contexthelp div');
    if ('undefined' === typeof value) {
      return label.text();
    }

    label.text(value);
    return this;
  };

  return ContextHelp;

});
